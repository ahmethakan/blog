<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Http\Request;
use Illuminate\Http\Response;


class StartCookies
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle(Request $request, Closure $next)
    {
        if(!$request->cookie('mode')) {
            $response = new Response();

            $response->withCookie(cookie()->forever('mode', 0));

            $response->send();
        }

        return $next($request);
    }
}
