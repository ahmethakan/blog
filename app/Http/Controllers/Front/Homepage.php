<?php

namespace App\Http\Controllers\Front;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Http\Response;

use Illuminate\Support\Facades\Auth;

use App\Models\Category;
use App\Models\Post;
use App\Models\Page;
use App\Models\Visitor;

class Homepage extends Controller
{

    public function __construct(Request $request)
    {
        // $pages = Page::where('status', '1')->orderBy('order')->get();
        // view()->share('pages', $pages);
        // view()->share('categories', Category::all());

        if( $request->cookie('mode') == NULL )
        {
            $response = new Response();

            $response->withCookie(cookie()->forever('mode', 0));

            $response->send();
        }
    }

    public function index(Request $request)
    {
        $data['posts'] = Post::where('status', '1')->orderBy('created_at', 'DESC')
                                                   ->paginate(10);

        $this->getVisitorDatas($request);

        return view('front.home', $data);
    }

    public function categorypage(Request $request, $ctg)
    {
        $site_address = 'http://127.0.0.1:8000';
        $data['category'] = Category::where('slug', $ctg)->first() ?? abort(404, $site_address);
        $data['posts'] = Post::where('category', $data['category']->id)
                            ->orderBy('created_at', 'DESC')
                            ->paginate(10);

        $this->getVisitorDatas($request);

        return view('front.category', $data);
    }

    public function pagepage(Request $request, $slug)
    {
        $site_address = 'http://127.0.0.1:8000';
        $data['content'] = Page::where('slug', $slug)->first() ?? abort(404, $site_address);

        $this->getVisitorDatas($request);

        return view('front.page', $data);
    }

    public function singlepage(Request $request, $ctg, $slug)
    {
        $site_address = 'http://127.0.0.1:8000';
        $post = Post::where('slug', $slug)->first() ?? abort(404, $site_address);
        $post->increment('hit');
        if($ctg === $post->getCategory->slug)
        {
            $data['post'] = $post;

            $this->getVisitorDatas($request);

            return view('front.single', $data);
        }
        else
        {
            $this->getVisitorDatas($request);

            abort(404, $site_address);
        }
    }


    public function getVisitorDatas(Request $request)
    {
        // $visitor = new Visitor;

        // $visitor->ip = $request->ip();
        // $visitor->browser = $request->userAgent();

        // $url = "";

        // foreach($request->segments() as $segment)
        // {
        //     $url = $url."/".$segment;
        // }

        // if( $url == NULL )
        // {
        //     $url = "/";
        // }

        // $visitor->url = $url;

        // $visitor->save();
    }

    public function changeMode(Request $request)
    {
        if( $_COOKIE['mode'] == 1 )
        {
            $response = new Response();

            $response->withCookie(cookie()->forever('mode', 0));

            $response->send();

            return response()->json([
                'mode' => 0
            ]);
        }
        else
        {
            $response = new Response();

            $response->withCookie(cookie()->forever('mode', 1));

            $response->send();

            return response()->json([
                'mode' => 1
            ]);
        }

    }
}
