<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

use Illuminate\Support\Str;

use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\File;

use App\Models\Post;
use App\Models\Category;

class Posts extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */

    public function index()
    {
        $data['user'] = Auth::user();

        $data['posts'] = Post::where('author', $data['user']->id)->orderBy('created_at', 'DESC')
                                            ->paginate(5);
        return view('admin.posts.index', $data);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $data['user'] = Auth::user();
        $data['categories'] = Category::all();

        return view('admin.posts.create', $data);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $data['user'] = Auth::user();

        if( $request->slug )
        {
            $slug = $request->slug;
        }
        else
        {
            $slug = Str::slug($request->title);
        }

        $post = Post::where('slug', $slug)->first();

        if(!$post)
        {
            if( $request->status == 1 )
            {
                $post = new Post;

                $post->category = $request->category;
                $post->author = $data['user']->id;
                $post->title = $request->title;
                $post->content = $request->content;
                $post->description = $request->description;
                $post->tags = $request->tags;

                if ( $request->hasFile('tumbnail') )
                {
                    $imageName = Str::slug($request->title).'-'.rand(11111, 99999).'.'.$request->file('tumbnail')->getClientOriginalExtension();
                    $destinationPath = 'gallery';
                    $upload_success = $request->file('tumbnail')->move($destinationPath, $imageName);
                    $post->tumbnail = '/'.$destinationPath.'/'.$imageName;
                }

                $post->slug = $slug;
                $post->status = "1";

                $post->save();

                if( $post )
                {
                    $category = Category::where('id', $request->category)->first();

                    if ( $category )
                    {
                        $category->total = $category->total + 1;

                        $category->save();

                        $data['messages'] = array(
                            'type' => 'success',
                            'text' => 'Post successfully published.'
                        );

                        $data['posts'] = Post::where('author', $data['user']->id)->orderBy('created_at', 'DESC')
                                                    ->paginate(5);

                        return view('admin.posts.index', $data);
                    }
                }

                // $data['messages'] = array(
                //                     'type' => 'success',
                //                     'text' => 'Post successfully published.'
                // );

                // $data['posts'] = Post::where('author', $data['user']->id)->orderBy('created_at', 'DESC')
                //                             ->paginate(5);

                // return view('admin.posts.index', $data);

            }
            else if( $request->status == 2 )
            {
                $post = new Post;

                $post->category = $request->category;
                $post->author = $data['user']->id;
                $post->title = $request->title;
                $post->content = $request->content;
                $post->description = $request->description;
                $post->tags = $request->tags;

                if ( $request->hasFile('tumbnail') )
                {
                    $imageName = Str::slug($request->title).'-'.rand(11111, 99999).'.'.$request->file('tumbnail')->getClientOriginalExtension();
                    $destinationPath = 'gallery';
                    $upload_success = $request->file('tumbnail')->move($destinationPath, $imageName);
                    $post->tumbnail = '/'.$destinationPath.'/'.$imageName;
                }

                if( $request->slug )
                {
                    $post->slug = $request->slug;
                }
                else
                {
                    $post->slug = Str::slug($request->title);
                }



                $post->status = "0";

                $post->save();

                $data['messages'] = array(
                    'type' => 'warning',
                    'text' => 'Post successfully drafted.'
                );

                $data['posts'] = Post::where('author', $data['user']->id)->orderBy('created_at', 'DESC')
                                            ->paginate(5);

                return view('admin.posts.index', $data);
            }

        }
        else
        {
            return back()->withInput()
                         ->withErrors('Post could not be shared.');
        }

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $data['user'] = Auth::user();
        $data['posts'] = Post::where('author', $data['user']->id)->orderBy('created_at', 'DESC')
                                            ->paginate(5);
        return view('admin.posts.index', $data);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $data['user'] = Auth::user();
        $data['categories'] = Category::all();
        $data['posts'] = Post::withTrashed()->Where('id', $id)
                                            ->get();

        return view('admin.posts.edit', $data);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $data['user'] = Auth::user();
        $post = Post::withTrashed()->find($id);

        if($post)
        {
            if( $request->status == 1 )
            {
                $oldcategory = $post->category;

                $post->category = $request->category;
                $post->author = $data['user']->id;
                $post->title = $request->title;
                $post->content = $request->content;
                $post->description = $request->description;
                $post->tags = $request->tags;

                if ( $request->hasFile('tumbnail') )
                {
                    $imageName = Str::slug($request->title).'-'.rand(11111, 99999).'.'.$request->file('tumbnail')->getClientOriginalExtension();
                    $destinationPath = 'gallery';
                    $upload_success = $request->file('tumbnail')->move($destinationPath, $imageName);
                    $post->tumbnail = '/'.$destinationPath.'/'.$imageName;
                }

                if( $request->slug )
                {
                    $post->slug = $request->slug;
                }
                else
                {
                    $post->slug = Str::slug($request->title);
                }

                $post->status = "1";

                if($post->deleted_at != NULL)
                {
                    $post->deleted_at = NULL;
                }

                $post->save();

                if( $post )
                {
                    $category = Category::where('id', $request->category)->first();

                    if ( $category )
                    {
                        $category->total = $category->total + 1;

                        $category->save();

                        if( $category )
                        {
                            $category = Category::where('id', $oldcategory)->first();

                            if ( $category )
                            {
                                $category->total = $category->total - 1;

                                $category->save();

                                $data['messages'] = array(
                                    'type' => 'success',
                                    'text' => 'Post successfully updated.'
                                );

                                $data['posts'] = Post::where('author', $data['user']->id)->orderBy('created_at', 'DESC')
                                                            ->paginate(5);

                                return view('admin.posts.index', $data);
                            }
                        }
                    }
                }

                // $data['messages'] = array(
                //                     'type' => 'success',
                //                     'text' => 'Post successfully updated.'
                // );

                // $data['posts'] = Post::where('author', $data['user']->id)->orderBy('created_at', 'DESC')
                //                             ->paginate(5);

                // return view('admin.posts.index', $data);

            }
            else if( $request->status == 2 )
            {
                $post->category = $request->category;
                $post->author = $data['user']->id;
                $post->title = $request->title;
                $post->content = $request->content;
                $post->description = $request->description;
                $post->tags = $request->tags;

                if ( $request->hasFile('tumbnail') )
                {
                    $imageName = Str::slug($request->title).'-'.rand(11111, 99999).'.'.$request->file('tumbnail')->getClientOriginalExtension();
                    $destinationPath = 'gallery';
                    $upload_success = $request->file('tumbnail')->move($destinationPath, $imageName);
                    $post->tumbnail = '/'.$destinationPath.'/'.$imageName;
                }

                if( $request->slug )
                {
                    $post->slug = $request->slug;
                }
                else
                {
                    $post->slug = Str::slug($request->title);
                }

                $post->status = "0";

                if($post->deleted_at != NULL)
                {
                    $post->deleted_at = NULL;
                }

                $post->save();

                $data['messages'] = array(
                    'type' => 'warning',
                    'text' => 'Post successfully drafted.'
                );

                $data['posts'] = Post::where('author', $data['user']->id)->orderBy('created_at', 'DESC')
                                            ->paginate(5);

                return view('admin.posts.index', $data);
            }

        }
        else
        {
            return back()->withInput()
                         ->withErrors('Post could not be updated.');
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function delete($id)
    {
        $data['user'] = Auth::user();
        $post = Post::find($id);
        if ( $post )
        {
            $post->status = 3;
            $post->save();

            $post->delete();

            $data['messages'] = array(
                'type' => 'success',
                'text' => 'Post successfully deleted.'
            );

            $data['posts'] = Post::where('author', $data['user']->id)->orderBy('created_at', 'DESC')
                                            ->paginate(5);

            return view('admin.posts.index', $data);
        }

        return back()->withInput()
                     ->withErrors('Post could not be deleted.');

    }

    public function trashed()
    {
        $data['user'] = Auth::user();
        $data['posts'] = Post::onlyTrashed()->orderBy('deleted_at', 'DESC')
                                            ->paginate(5);

        // return $data['posts'];
        return view('admin.posts.trash', $data);
    }

    public function destroy($id)
    {
        $data['user'] = Auth::user();
        $post = Post::onlyTrashed()->find($id);
        if ( $post )
        {
            if( File::exists( public_path($post->tumbnail)) )
            {
                File::delete( public_path($post->tumbnail));
            }

            $post->forceDelete();

            return redirect()->back()->withErrors('Post successfully destroyed.');
        }

        return redirect()->back()->withErrors('Post could not be destroyed.');
    }
}
