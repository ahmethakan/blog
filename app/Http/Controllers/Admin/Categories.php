<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

use Illuminate\Support\Str;

use Illuminate\Support\Facades\Auth;

use App\Models\Category;
use App\Models\Post;

class Categories extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $data['user'] = Auth::user();

        $data['categories'] = Category::orderBy('created_at', 'DESC')->paginate(5);

        return view('admin.categories.index', $data);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $data['user'] = Auth::user();

        if( $request->categoryslug )
        {
            $slug = $request->categoryslug;
        }
        else
        {
            $slug = Str::slug($request->categoryname);
        }

        $category = Category::withTrashed()->where('slug', $slug)->first();

        if(!$category)
        {
            $category = new Category;

            $category->name = $request->categoryname;
            $category->slug = $slug;
            $category->total = 0;

            $category->save();

            $data['messages'] = array(
                'type' => 'success',
                'text' => 'Category successfully created.'
            );

            $data['categories'] = Category::orderBy('created_at', 'DESC')->paginate(5);

            return view('admin.categories.index', $data);
        }
        else
        {
            $data['messages'] = array(
                'type' => 'danger',
                'text' => 'Category could not create.'
            );

            $data['categories'] = Category::orderBy('created_at', 'DESC')->paginate(5);

            return view('admin.categories.index', $data);
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $data['user'] = Auth::user();

        $category = Category::find($id);

        if ( $category )
        {
            $category->delete();

            if ( $category )
            {
                $posts = Post::where('category', $id)->get();

                foreach( $posts as $post)
                {
                    $post->status = 3;
                    $post->slug = NULL;
                    $post->save();

                    $post->delete();
                }

                $data['messages'] = array(
                    'type' => 'success',
                    'text' => 'Post successfully deleted.'
                );

                $data['categories'] = Category::orderBy('created_at', 'DESC')->paginate(5);

                return view('admin.categories.index', $data);
            }
        }
    }
}
