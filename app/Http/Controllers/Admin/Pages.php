<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

use Illuminate\Support\Str;

use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\File;

use App\Models\Page;

class Pages extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $data['user'] = Auth::user();

        $data['pages'] = Page::orderBy('order')
                                        ->paginate(5);

        return view('admin.pages.index', $data);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $data['user'] = Auth::user();

        return view('admin.pages.create', $data);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $data['user'] = Auth::user();

        if( $request->slug )
        {
            $slug = $request->slug;
        }
        else
        {
            $slug = Str::slug($request->title);
        }

        $page = Page::where('slug', $slug)->first();

        if(!$page)
        {
            if( $request->status == 1 )
            {
                $page = new Page;

                $page->title = $request->title;
                $page->content = $request->content;
                $page->description = $request->description;
                $page->tags = $request->tags;

                if ( $request->hasFile('tumbnail') )
                {
                    $imageName = Str::slug($request->title).'-'.rand(11111, 99999).'.'.$request->file('tumbnail')->getClientOriginalExtension();
                    $destinationPath = 'gallery';
                    $upload_success = $request->file('tumbnail')->move($destinationPath, $imageName);
                    $page->tumbnail = '/'.$destinationPath.'/'.$imageName;
                }

                $page->slug = $slug;
                $page->order = $request->order;
                $page->status = "1";

                if($page->deleted_at != NULL)
                {
                    $page->deleted_at = NULL;
                }

                $page->save();

                if( $page )
                {
                    $data['messages'] = array(
                        'type' => 'success',
                        'text' => 'Page successfully created.'
                    );

                    $data['pages'] = Page::orderBy('order')
                                                ->paginate(5);

                    return view('admin.pages.index', $data);
                }

            }
            else if( $request->status == 2 )
            {
                $page = new Page;

                $page->title = $request->title;
                $page->content = $request->content;
                $page->description = $request->description;
                $page->tags = $request->tags;

                if ( $request->hasFile('tumbnail') )
                {
                    $imageName = Str::slug($request->title).'-'.rand(11111, 99999).'.'.$request->file('tumbnail')->getClientOriginalExtension();
                    $destinationPath = 'gallery';
                    $upload_success = $request->file('tumbnail')->move($destinationPath, $imageName);
                    $page->tumbnail = '/'.$destinationPath.'/'.$imageName;
                }

                $page->slug = $slug;
                $page->order = $request->order;
                $page->status = "0";

                if($page->deleted_at != NULL)
                {
                    $page->deleted_at = NULL;
                }

                $page->save();

                $data['messages'] = array(
                    'type' => 'warning',
                    'text' => 'Page successfully drafted.'
                );

                $data['pages'] = Page::orderBy('order')
                                            ->paginate(5);

                return view('admin.pages.index', $data);
            }

        }
        else
        {
            return back()->withInput()
                         ->withErrors('Page could not be created.');
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $data['user'] = Auth::user();

        $data['pages'] = Page::withTrashed()->Where('id', $id)
                                            ->get();

        return view('admin.pages.edit', $data);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $data['user'] = Auth::user();

        $page = Page::withTrashed()->find($id);

        if($page)
        {
            if( $request->status == 1 )
            {
                $page->title = $request->title;
                $page->content = $request->content;
                $page->description = $request->description;
                $page->tags = $request->tags;

                if ( $request->hasFile('tumbnail') )
                {
                    $imageName = Str::slug($request->title).'-'.rand(11111, 99999).'.'.$request->file('tumbnail')->getClientOriginalExtension();
                    $destinationPath = 'gallery';
                    $upload_success = $request->file('tumbnail')->move($destinationPath, $imageName);
                    $page->tumbnail = '/'.$destinationPath.'/'.$imageName;
                }

                if( $request->slug )
                {
                    $page->slug = $request->slug;
                }
                else
                {
                    $page->slug = Str::slug($request->title);
                }

                $page->order = $request->order;
                $page->status = "1";

                if($page->deleted_at != NULL)
                {
                    $page->deleted_at = NULL;
                }

                $page->save();

                if( $page )
                {
                    $data['messages'] = array(
                        'type' => 'success',
                        'text' => 'Page successfully updated.'
                    );

                    $data['pages'] = Page::orderBy('order')
                                                ->paginate(5);

                    return view('admin.pages.index', $data);
                }

            }
            else if( $request->status == 2 )
            {
                $page->title = $request->title;
                $page->content = $request->content;
                $page->description = $request->description;
                $page->tags = $request->tags;

                if ( $request->hasFile('tumbnail') )
                {
                    $imageName = Str::slug($request->title).'-'.rand(11111, 99999).'.'.$request->file('tumbnail')->getClientOriginalExtension();
                    $destinationPath = 'gallery';
                    $upload_success = $request->file('tumbnail')->move($destinationPath, $imageName);
                    $page->tumbnail = '/'.$destinationPath.'/'.$imageName;
                }

                if( $request->slug )
                {
                    $page->slug = $request->slug;
                }
                else
                {
                    $page->slug = Str::slug($request->title);
                }

                $page->order = $request->order;
                $page->status = "0";

                if($page->deleted_at != NULL)
                {
                    $page->deleted_at = NULL;
                }

                $page->save();

                $data['messages'] = array(
                    'type' => 'warning',
                    'text' => 'Page successfully drafted.'
                );

                $data['pages'] = Page::orderBy('order')
                                            ->paginate(5);

                return view('admin.pages.index', $data);
            }

        }
        else
        {
            return back()->withInput()
                         ->withErrors('Page could not be updated.');
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function delete($id)
    {
        $data['user'] = Auth::user();
        $page = Page::find($id);
        if ( $page )
        {
            $page->status = 3;
            $page->save();

            $page->delete();

            $data['messages'] = array(
                'type' => 'success',
                'text' => 'Page successfully deleted.'
            );

            $data['pages'] = Page::orderBy('order')
                                        ->paginate(5);

            return view('admin.pages.index', $data);
        }

        return back()->withInput()
                     ->withErrors('Page could not be deleted.');

    }

    public function trashed()
    {
        $data['user'] = Auth::user();
        $data['pages'] = Page::onlyTrashed()->orderBy('deleted_at', 'DESC')
                                            ->paginate(5);

        // return $data['posts'];
        return view('admin.pages.trash', $data);
    }

    public function destroy($id)
    {
        $data['user'] = Auth::user();
        $page = Page::onlyTrashed()->find($id);
        if ( $page )
        {
            if( File::exists( public_path($page->tumbnail)) )
            {
                File::delete( public_path($page->tumbnail));
            }

            $page->forceDelete();

            return redirect()->back()->withErrors('Page successfully destroyed.');
        }

        return redirect()->back()->withErrors('Page could not be destroyed.');
    }
}
