<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

use Illuminate\Support\Str;

use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\File;

class Gallery extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $data['user'] = Auth::user();

        $data['images'] = File::files(public_path('gallery/'));

        // return $data['images'][0]->getRelativePathname();

        return view('admin.gallery.index', $data);

    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $data['user'] = Auth::user();

        if ( $request->hasFile('galleryimage') )
        {
            $imageName = 'image'.'-'.rand(11111, 99999).'.'.$request->file('galleryimage')->getClientOriginalExtension();
            $destinationPath = 'gallery';
            $upload_success = $request->file('galleryimage')->move($destinationPath, $imageName);

            if( $upload_success )
            {
                $data['messages'] = array(
                    'type' => 'success',
                    'text' => 'Image successfully updated.',
                    'name' => $imageName,
                    'link' => 'http://127.0.0.1:8000/'.$upload_success
                );
            }
            else
            {
                $data['messages'] = array(
                    'type' => 'danger',
                    'text' => 'Image could not updated.',
                    'name' => NULL,
                    'link' => NULL
                );
            }
        }
        else
        {
            $data['messages'] = array(
                'type' => 'danger',
                'text' => 'Image could not updated.',
                'name' => NULL,
                'link' => NULL
            );
        }

        $data['images'] = File::files(public_path('gallery/'));

        return view('admin.gallery.index', $data);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $data['user'] = Auth::user();

        File::delete(public_path('gallery/'. $id));

        $data['images'] = File::files(public_path('gallery'));

        return view('admin.gallery.index', $data);
    }
}
