<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

use App\Models\User;
use App\Models\Setting;

class Dashboard extends Controller
{
    public function index()
    {
        $data['user'] = Auth::user();

        return view('admin.dashboard', $data);

    }

    public function login()
    {
        return view('admin.users.login');
    }

    public function loginPost(Request $request)
    {

        if (Auth::guard('web')->attempt(['email' => $request->blg_email, 'password' => $request->blg_password])) {
            $request->session()
                    ->regenerate();

            return redirect()
                ->route('dashboard');
        }

        return back()->withErrors([
            'email' => 'Your email or password is incorrect.',
        ]);
    }

    public function logout()
    {
        if (Auth::check())
        {
            Auth::guard('web')
                ->logout();
        }

        return redirect()
            ->route('login');
    }

    public function create(Request $request)
    {
        return view('admin.users.create');
    }

    public function createUserPost(Request $request)
    {
        $user = User::where('email', $request->email)->first();

        if( $user )
        {
            return response()->json([
                'success'=>'false',
                'message'=>'This user already exists.'
            ]);
        }

        $user = new User;

        $user->name = $request->name;
        $user->authorname = $request->name;
        $user->email = $request->email;
        $user->phone = $request->mobile_number;
        $user->password = bcrypt($request->password);

        $user->save();

        if( $user )
        {
            return response()->json([
                'success'=>'true',
                'message'=>'User successfully created.'
            ]);
        }
        else
        {
            return response()->json([
                'success'=>'true',
                'message'=>'Burada.'
            ]);
        }
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        if (Auth::check())
        {
            $user = User::where('id', $id)->first();

            $user->ip_address = $request->user_ip_address;
            $user->name = $request->user_name;
            $user->authorname = $request->user_authorname;
            $user->username = $request->user_username;
            $user->email = $request->user_email;
            $user->password = bcrypt($request->user_password);
            $user->picture = $request->user_picture;
            $user->about = $request->user_about;
            $user->phone = $request->user_phone;
            $user->job = $request->user_job;
            $user->location = $request->user_location;

            $user->save();

            Auth::guard('web')
                ->logout();


            if (Auth::guard('web')->attempt(['email' => $request->user_email, 'password' => $request->user_password])) {
                $request->session()
                        ->regenerate();

                return redirect()
                    ->route('settings.index');
            }
        }

    }

}
