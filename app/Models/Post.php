<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;


class Post extends Model
{
    use HasFactory, SoftDeletes;

    protected $table = 'blg_posts';

    public function getCategory()
    {
        return $this->hasOne(Category::class, 'id', 'category')->withTrashed();
    }

    public function getAuthor()
    {
        return $this->hasOne(User::class, 'id', 'author');
    }

}
