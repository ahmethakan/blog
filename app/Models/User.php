<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

use Illuminate\Foundation\Auth\User as Authenticatable;

class User extends Authenticatable
{
    use HasFactory;

    protected $table = 'blg_users';

    protected $guard = 'web';

    protected $fillable = [
        'name', 'username', 'password',
    ];

    protected $hidden = [
        'password', 'remember_token',
    ];

    // public function getAuthorPosts()
    // {
    //     return $this->hasMany(Post::class, 'author', 'id')
    //                 ->orderBy('created_at', 'DESC')
    //                 ->paginate(5);
    // }

    public function getAuthorPostsCount()
    {
        return $this->hasMany(Post::class, 'author', 'id')
                    ->orderBy('created_at', 'DESC')
                    ->get()
                    ->count();
    }

    public function getAuthorDraftedPostsCount()
    {
        return $this->hasMany(Post::class, 'author', 'id')
                    ->where('status', 0)
                    ->get()
                    ->count();
    }

    public function getAuthorTrashedPostsCount()
    {
        return $this->hasMany(Post::class, 'author', 'id')
                    ->withTrashed()
                    ->where('status', 3)
                    ->get()
                    ->count();
    }

    public function getAllPostsCount()
    {
        return Post::all()->count();
    }

    public function getAllUsersCount()
    {
        return User::all()->count();
    }



}
