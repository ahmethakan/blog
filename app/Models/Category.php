<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;


class Category extends Model
{
    use HasFactory, SoftDeletes;

    protected $table = 'blg_categories';

    public function getPosts()
    {
        return $this->hasMany(Post::class, 'category', 'id')->orderBy('created_at', 'DESC');
    }
}
