<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class Visitors extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('blg_visitors', function (Blueprint $table) {
            $table->id();
            $table->string('ip')->nullable()->default(NULL);
            $table->string('url')->nullable()->default(NULL);
            $table->string('city')->nullable()->default(NULL);
            $table->string('browser')->nullable()->default(NULL);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
