<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class Users extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('blg_users', function (Blueprint $table) {
            $table->id();
            $table->string('ip_address')->nullable()->default(NULL);
            $table->string('name');
            $table->string('authorname')->nullable()->default(NULL);
            $table->string('username')->nullable()->default(NULL);
            $table->string('email');
            $table->string('password');
            $table->string('picture')->nullable()->default(NULL);
            $table->string('about')->nullable()->default(NULL);
            $table->string('phone');
            $table->string('job')->nullable()->default(NULL);
            $table->string('location')->nullable()->default(NULL);
            $table->integer('verified')->default(0);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('blg_users');
    }
}
