<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateSettingsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('blg_settings', function (Blueprint $table) {
            $table->id();
            $table->string('site_title');
            $table->string('site_address');
            $table->longText('site_description');
            $table->string('site_tags');
            $table->string('site_copyright');
            $table->string('site_favicon');
            $table->string('blog_name');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('settings');
    }
}
