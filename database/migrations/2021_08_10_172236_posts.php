<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class Posts extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('blg_posts', function (Blueprint $table) {
            $table->id();
            $table->bigInteger('category')->unsigned();
            $table->bigInteger('author')->unsigned();
            $table->string('title');
            $table->string('slug');
            $table->string('tumbnail')->nullable()->default(NULL);
            $table->longText('content');
            $table->string('description');
            $table->string('tags');
            $table->integer('hit')->default(0);
            $table->integer('status')->default(0);
            $table->softDeletes();
            $table->timestamps();
        });

        Schema::table('blg_posts', function(Blueprint $table) {

            $table->foreign('author')
                  ->references('id')
                  ->on('blg_users')
                  ->onDelete('cascade');

            $table->foreign('category')
                  ->references('id')
                  ->on('blg_categories')
                  ->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('blg_posts');
    }
}
