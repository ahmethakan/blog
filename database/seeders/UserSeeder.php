<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;

use Faker\Factory as Faker;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Str;

class UserSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $faker = Faker::create();
        for($i=0; $i<4; $i++)
        {
            $name = $faker->name();
            DB::table('blg_users')->insert([
                'name' => $name,
                'authorname' => $name,
                'username' => Str::slug($name),
                'email' => $faker->email(),
                'password' => bcrypt('12345'),
                'picture' => $faker->imageUrl(120, 120, 'cats'),
                'about' => $faker->text(200),
                'phone' => $faker->phoneNumber(),
                'job' => $faker->jobTitle(),
                'location' => $faker->name(),
                'created_at' => $faker->dateTime(),
                'updated_at' => $faker->dateTime()

            ]);
        }

        DB::table('blg_users')->insert([
            'name' => 'Ahmet Çelik',
            'authorname' => 'Ahmet',
            'username' => 'ahmethakan',
            'email' => 'ahmet@ahmet.com',
            'password' => bcrypt('123456'),
            'picture' => $faker->imageUrl(120, 120, 'cats'),
            'about' => $faker->text(200),
            'phone' => $faker->phoneNumber(),
            'job' => $faker->jobTitle(),
            'location' => $faker->name(),
            'verified' => 1,
            'created_at' => $faker->dateTime(),
            'updated_at' => $faker->dateTime()

        ]);
    }
}
