<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;

use Faker\Factory as Faker;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Str;

class PostSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $faker = Faker::create();
        for($i=0; $i<28; $i++)
        {
            $title = $faker->sentence();
            DB::table('blg_posts')->insert([
                'category' => rand(1,4),
                'author' => rand(1,5),
                'title' => $title,
                'slug' => Str::slug($title),
                'tumbnail' => $faker->imageUrl(720, 360, 'cats', true, 'Faker'),
                'content' => $faker->text(1600),
                'description' => $faker->text(250),
                'tags' => $faker->word().','.$faker->word().','.$faker->word(),
                'status' => rand(0,1),
                'created_at' => $faker->dateTime(),
                'updated_at' => $faker->dateTime()
            ]);
        }
    }
}
