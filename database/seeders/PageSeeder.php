<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;

use Faker\Factory as Faker;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Str;

class PageSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $faker = Faker::create();
        for($i=0; $i<10; $i++)
        {
            $title = $faker->word();
            DB::table('blg_pages')->insert([
                'title' => $title,
                'slug' => Str::slug($title),
                'tumbnail' => $faker->imageUrl(640, 280, 'animals', true),
                'content' => $faker->text(1600),
                'description' => $faker->text(250),
                'tags' => $faker->word().','.$faker->word().','.$faker->word(),
                'order' => $i,
                'status' => rand(0,1),
                'created_at' => $faker->dateTime(),
                'updated_at' => $faker->dateTime()
            ]);
        }
    }
}
