<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Str;



class CategorySeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('blg_categories')->insert([
            'name' => 'C++',
            'slug' => Str::slug('CPP'),
            'total' => 10
        ]);
        DB::table('blg_categories')->insert([
            'name' => 'PHP',
            'slug' => Str::slug('PHP'),
            'total' => 8
        ]);

        DB::table('blg_categories')->insert([
            'name' => 'Genel',
            'slug' => Str::slug('Genel'),
            'total' => 14
        ]);

        DB::table('blg_categories')->insert([
            'name' => 'Gömülü Sistemler',
            'slug' => Str::slug('Gömülü Sistemler'),
            'total' => 6
        ]);
    }
}
