@extends('admin.layouts.master')

@section('site_title', 'Blog')
@section('site_section', 'Gallery')


@section('title', 'Gallery')


@section('blog_name', 'AHC')
@section('site_address', 'http://127.0.0.1:8000')

@section('copyright', 'Copyright &copy; 2018 <div class="bullet"></div> All rights reserved.')

@section('dashboard')

<div class="section-body">
  <div class="row mt-4">
    <div class="col-12 col-md-12 col-lg-4">
      <div class="card">
        <div class="card-header">
          <h4>Upload İmage</h4>
        </div>
        <div class="card-body">
          <form method="post" action="{{route('gallery.store')}}" enctype="multipart/form-data">
            @csrf
            <div class="form-group">
              <label>File</label>
              <input type="file" class="form-control" name="galleryimage" value="{{ old('categoryslug') ?? '' }}">
            </div>
            <div class="form-group text-right">
                <button type="submit" class="btn btn-primary btn-lg btn-icon icon-right" name="submit">
                    Upload
                </button>
            </div>
        </form>
        </div>
      </div>
    </div>
    <div class="col-12 col-md-12 col-lg-8">
      @if ($messages ?? '')
        <div class="alert alert-{{$messages['type'] ?? ''}} alert-dismissible show fade">
          <div class="alert-body">
            <button class="close" data-dismiss="alert">
              <span>×</span>
            </button>
            {{$messages['text'] ?? ''}}
            @if( $messages['link'] )
            <p id="link" onclick="copy()">{{$messages['link'] ?? ''}} <i class="fas fa-copy"></i></p>
            <script>
              function copy() {
                var copyText = document.getElementById("link").innerText;
                var elem = document.createElement("textarea");
                document.body.appendChild(elem);
                elem.value = copyText;
                elem.select();
                document.execCommand("copy");
                document.body.removeChild(elem);
                alert("Copied the text");
              }
            </script>
            @endif
          </div>
        </div>
      @endif
      <div class="card">
        <div class="card-header">
          <h4>Gallery</h4>
        </div>
        <div class="card-body">
          <div class="gallery gallery-md">
            @foreach ($images as $image)
            <div class="gallery-item" data-image="{{'/gallery/'.$image->getRelativePathname()}}"
              data-title="{{$image->getRelativePathname()}}" href="{{'/gallery/'.$image->getRelativePathname()}}"
              title="{{$image->getRelativePathname()}}" style="height:175px;width:175px;padding:5px;background-image: url(&quot;{{'/gallery/'.$image->getRelativePathname()}}&quot;);">
              <a href="{{ route('gallery.destroy', ['gallery'=> $image->getRelativePathname()]) }}"
              class="btn btn-icon btn-sm btn-danger" onclick="event.preventDefault();
              document.getElementById('delete-form-{{$image->getRelativePathname()}}').submit();">
                <i class="fas fa-trash-alt"></i>
              </a>
              <form id="delete-form-{{$image->getRelativePathname()}}" action="{{ route('gallery.destroy', ['gallery'=> $image->getRelativePathname()]) }}"
                method="POST" style="display: none;">
                @method('DELETE');
                @csrf
              </form>
            </div>
            @endforeach
          </div>
        </div>
      </div>
    </div>
  </div>
</div>
@endsection
