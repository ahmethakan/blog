@extends('admin.layouts.master')

@section('site_title', 'Blog')
@section('site_section', 'Blog')


@section('title', 'Trash')


@section('blog_name', 'AHC')
@section('site_address', 'http://127.0.0.1:8000')

@section('copyright', 'Copyright &copy; 2018 <div class="bullet"></div> All rights reserved.')

@section('dashboard')

@foreach ($errors->all() as $error)
  <div class="alert alert-danger alert-dismissible show fade">
    <div class="alert-body">
      <button class="close" data-dismiss="alert">
        <span>×</span>
      </button>
      {{$error}}
    </div>
  </div>
@endforeach


<div class="section-body">
  <div class="row mt-4">
    <div class="col-12">
      @if ($messages ?? '')
        <div class="alert alert-{{$messages['type'] ?? ''}} alert-dismissible show fade">
          <div class="alert-body">
            <button class="close" data-dismiss="alert">
              <span>×</span>
            </button>
            {{$messages['text'] ?? ''}}
          </div>
        </div>
      @endif
      <div class="card">
        <div class="card-header">
          <h4>
            Trashed Posts
          </h4>
          @if ( $pages[0] == NULL )
              <div class='text-danger'>Empty</div>
          @endif
        </div>
        <div class="card-body">
          <div class="float-left">
            <select class="form-control selectric">
              <option>Action For Selected</option>
              <option>Move to Draft</option>
              <option>Move to Pending</option>
              <option>Delete Permanently</option>
            </select>
          </div>
          <div class="float-right">
            <form>
              <div class="input-group">
                <input type="text" class="form-control" placeholder="Search">
                <div class="input-group-append">
                  <button class="btn btn-primary"><i class="fas fa-search"></i></button>
                </div>
              </div>
            </form>
          </div>
          <div class="clearfix mb-3"></div>
          <div class="table-responsive">
            <table class="table table-striped">
              <tr>
                <th class="pt-2">
                  <div class="custom-checkbox custom-checkbox-table custom-control">
                    <input type="checkbox" data-checkboxes="mygroup" data-checkbox-role="dad"
                      class="custom-control-input" id="checkbox-all">
                    <label for="checkbox-all" class="custom-control-label">&nbsp;</label>
                  </div>
                </th>
                <th>Title</th>
                <th>Created At</th>
                <th>Views</th>
                <th>Order</th>
                <th>Status</th>
              </tr>
              @foreach ($pages as $item)
              <tr>
                <td>
                  <div class="custom-checkbox custom-control">
                    <input type="checkbox" data-checkboxes="mygroup" class="custom-control-input"
                      id="{{$item->id}}">
                    <label for="{{$item->id}}" class="custom-control-label">&nbsp;</label>
                  </div>
                </td>
                <td>{{\Illuminate\Support\Str::limit($item->title, 60)}}
                  <div class="table-links">
                    <a href="{{route('pagepage',[$item->slug])}}">View</a>
                    <div class="bullet"></div>
                    <a href="{{route('pages.edit', ['page'=> $item->id])}}">Edit</a>
                    <div class="bullet"></div>
                    <a href="{{ route('pages.destroy', ['page'=> $item->id]) }}"
                    class="text-danger" onclick="event.preventDefault();
                    document.getElementById('delete-form-{{ $item->id }}').submit();">
                      Destroy
                    </a>
                    <form id="delete-form-{{ $item->id }}" action="{{ route('pages.destroy', ['page'=> $item->id]) }}"
                      method="POST" style="display: none;">
                      @method('DELETE');
                      @csrf
                    </form>
                </td>

                <td>{{$item->created_at}}</td>
                <td>{{$item->hit}}</td>
                <td>{{$item->order}}</td>
                <td>
                  @if ($item->deleted_at != NULL)
                  <span class="badge badge-danger">Deleted</span>
                  @endif
                </td>
              </tr>
              @endforeach
            </table>
          </div>

          <div class="float-right">
            <nav>
                {{$pages->links("pagination::bootstrap-4")}}
            </nav>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>

@endsection
