@extends('admin.layouts.master')

@section('site_title', 'Blog')
@section('site_section', 'Blog')


@section('title', 'New Page')


@section('blog_name', 'AHC')
@section('site_address', 'http://127.0.0.1:8000')

@section('copyright', 'Copyright &copy; 2018 <div class="bullet"></div> All rights reserved.')

@section('dashboard')

            @foreach ($errors->all() as $error)
            <div class="alert alert-danger alert-dismissible show fade">
              <div class="alert-body">
                <button class="close" data-dismiss="alert">
                  <span>×</span>
                </button>
                {{$error}}
              </div>
            </div>
            @endforeach
            <div class="section-body">
                <div class="row">
                    <div class="col-12">
                        <div class="card">
                            <div class="card-body">
                                <form method="POST" action="{{route('pages.store')}}" enctype="multipart/form-data">
                                    @csrf
                                    <div class="form-group row mb-4">
                                        <label class="col-form-label text-md-right col-12 col-md-2 col-lg-2">Title</label>
                                        <div class="col-sm-12 col-md-8">
                                            <input type="text" class="form-control" name="title" value="{{ old('title') ?? '' }}" autocomplete="off">
                                        </div>
                                    </div>
                                    <div class="form-group row mb-4">
                                        <label class="col-form-label text-md-right col-12 col-md-2 col-lg-2">Content</label>
                                        <div class="col-sm-12 col-md-8">
                                            <textarea id="froala" name="content">{{ old('content') ?? '' }}</textarea>
                                        </div>
                                    </div>
                                    <div class="form-group row mb-4">
                                        <label class="col-form-label text-md-right col-12 col-md-2 col-lg-2">Description</label>
                                        <div class="col-sm-12 col-md-8">
                                            <textarea class="form-control" name="description" style="height:100px">{{ old('description') ?? '' }}</textarea>
                                        </div>
                                    </div>
                                    <div class="form-group row mb-4">
                                        <label class="col-form-label text-md-right col-12 col-md-2 col-lg-2">Tags</label>
                                        <div class="col-sm-12 col-md-8">
                                            <input type="text" class="form-control" name="tags" value="{{ old('tags') ?? '' }}" autocomplete="off">
                                        </div>
                                    </div>
                                    <div class="form-group row mb-4">
                                        <label class="col-form-label text-md-right col-12 col-md-2 col-lg-2">Tumbnail</label>
                                        <div class="col-sm-12 col-md-8">
                                            <div id="image-preview" class="image-preview">
                                                <label for="image-upload" id="image-label">Choose File</label>
                                                <input type="file" name="tumbnail" id="image-upload" />
                                            </div>
                                        </div>
                                    </div>
                                    <div class="form-group row mb-4">
                                        <label class="col-form-label text-md-right col-12 col-md-2 col-lg-2">Slug</label>
                                        <div class="col-sm-12 col-md-8">
                                            <input type="text" class="form-control" name="slug" value="{{ old('slug') ?? '' }}" autocomplete="off">
                                        </div>
                                    </div>
                                    <div class="form-group row mb-4">
                                        <label class="col-form-label text-md-right col-12 col-md-2 col-lg-2">Order</label>
                                        <div class="col-sm-12 col-md-8">
                                            <input type="text" class="form-control" name="order" value="{{ old('order') ?? '' }}" autocomplete="off">
                                        </div>
                                    </div>
                                    <div class="form-group row mb-4">
                                        <label class="col-form-label text-md-right col-12 col-md-2 col-lg-2">Status</label>
                                        <div class="col-sm-12 col-md-8">
                                            <select class="form-control" name="status">
                                                <option value="1">Publish</option>
                                                <option value="2">Draft</option>
                                            </select>
                                        </div>
                                    </div>
                                    <div class="form-group row mb-4">
                                        <label class="col-form-label text-md-right col-12 col-md-2 col-lg-2"></label>
                                        <div class="col-sm-12 col-md-8">
                                            <button type="submit" class="btn btn-primary btn-lg btn-icon icon-right" name="submit">
                                                Create
                                            </button>
                                        </div>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

@endsection

@section('summernotecss')
<link href="https://cdn.jsdelivr.net/npm/froala-editor@latest/css/froala_editor.pkgd.min.css" rel="stylesheet" type="text/css" />
@endsection

@section('summernotejs')
<script type="text/javascript" src="https://cdn.jsdelivr.net/npm/froala-editor@latest/js/froala_editor.pkgd.min.js"></script>


<script>
    var editor = new FroalaEditor('#froala', {
        height: 300,
        quickInsertEnabled: false
    });
</script>

@endsection