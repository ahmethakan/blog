<!DOCTYPE html>
<html lang="tr">


<!-- blank.html  21 Nov 2019 03:54:41 GMT -->
<head>
  <meta charset="UTF-8">
  <meta content="width=device-width, initial-scale=1, maximum-scale=1, shrink-to-fit=no" name="viewport">
  <title>@yield('title') - @yield('site_title')</title>
  <!-- General CSS Files -->
  <link rel="stylesheet" href="{{asset('front/')}}/css/app.min.css">
  <!-- Template CSS -->
  <link rel="stylesheet" href="{{asset('front/')}}/css/style.css">
  <link rel="stylesheet" href="{{asset('front/')}}/css/components.css">
  <!-- Custom style CSS -->
  <link rel="stylesheet" href="{{asset('front/')}}/css/custom.css">
  <link rel='shortcut icon' type='image/x-icon' href='{{asset('front/')}}/img/favicon.ico' />

  @yield('summernotecss')

</head>

<body>
  @yield('modal')
  <div class="loader"></div>
  <div id="app">
    <div class="main-wrapper main-wrapper-1">
      <div class="navbar-bg"></div>
      <nav class="navbar navbar-expand-lg main-navbar sticky">
        <div class="form-inline mr-auto">
          <ul class="navbar-nav mr-3">
            <li><a href="#" data-toggle="sidebar" class="nav-link nav-link-lg
									collapse-btn"> <i data-feather="align-justify"></i></a>
            </li>
          </ul>
        </div>
        <ul class="navbar-nav navbar-right">
          <li class="dropdown"><a href="http://127.0.0.1:8000"
              class="nav-link nav-link-lg"><i data-feather="eye"></i></a>
          </li>
          <li class="dropdown dropdown-list-toggle"><a href="#" data-toggle="dropdown"
              class="nav-link nav-link-lg message-toggle"><i data-feather="mail"></i>
              <span class="badge headerBadge1">
                1 </span> </a>
            <div class="dropdown-menu dropdown-list dropdown-menu-right pullDown">
              <div class="dropdown-header">
                Messages
                <div class="float-right">
                  <a href="#">Mark All As Read</a>
                </div>
              </div>
              <div class="dropdown-list-content dropdown-list-message">
                <a href="#" class="dropdown-item"> <span class="dropdown-item-avatar
											text-white"> <img alt="image" src="assets/img/users/user-1.png" class="rounded-circle">
                  </span> <span class="dropdown-item-desc"> <span class="message-user">John
                      Deo</span>
                    <span class="time messege-text">Please check your mail !!</span>
                    <span class="time">2 Min Ago</span>
                  </span>
                </a>
              </div>
              <div class="dropdown-footer text-center">
                <a href="#">View All <i class="fas fa-chevron-right"></i></a>
              </div>
            </div>
          </li>
          <li class="dropdown dropdown-list-toggle"><a href="#" data-toggle="dropdown"
              class="nav-link notification-toggle nav-link-lg"><i data-feather="bell" class="bell"></i>
            </a>
            <div class="dropdown-menu dropdown-list dropdown-menu-right pullDown">
              <div class="dropdown-header">
                Notifications
                <div class="float-right">
                  <a href="#">Mark All As Read</a>
                </div>
              </div>
              <div class="dropdown-list-content dropdown-list-icons">
                <a href="#" class="dropdown-item dropdown-item-unread"> <span
                    class="dropdown-item-icon bg-primary text-white"> <i class="fas
												fa-code"></i>
                  </span> <span class="dropdown-item-desc"> Template update is
                    available now! <span class="time">2 Min
                      Ago</span>
                  </span>
                </a>
              </div>
              <div class="dropdown-footer text-center">
                <a href="#">View All <i class="fas fa-chevron-right"></i></a>
              </div>
            </div>
          </li>
          <li class="dropdown">
            <a href="#" data-toggle="dropdown"
            class="nav-link dropdown-toggle nav-link-lg nav-link-user">
            @if ( Auth::user()->picture == NULL )
            <img alt="image" src="{{asset('front/')}}/img/favicon.ico"
              class="user-img-radious-style">
            @else
            <img alt="image" src="{{ Auth::user()->picture }}"
              class="user-img-radious-style">
            @endif
            <span class="d-sm-none d-lg-inline-block"></span></a>
            <div class="dropdown-menu dropdown-menu-right pullDown">
              <div class="dropdown-title">Hi, {{$user->name}}</div>
              <a href="profile.html" class="dropdown-item has-icon"> <i class="far
										fa-user"></i> Profile
              </a> <a href="timeline.html" class="dropdown-item has-icon"> <i class="fas fa-bolt"></i>
                Activities
              </a> <a href="#" class="dropdown-item has-icon"> <i class="fas fa-cog"></i>
                Settings
              </a>
              <div class="dropdown-divider"></div>
              <a href="{{route('logout')}}" class="dropdown-item has-icon text-danger"> <i class="fas fa-sign-out-alt"></i>
                Logout
              </a>
            </div>
          </li>
        </ul>
      </nav>
      <div class="main-sidebar sidebar-style-2">
        <aside id="sidebar-wrapper">
          <div class="sidebar-brand">
            <a href="{{route('dashboard')}}">
              <img alt="image" src="{{asset('front/')}}/img/logo.png" class="header-logo" />
            </a>
          </div>
          <ul class="sidebar-menu">
            <li class="menu-header">MAIN</li>
            <li class="dropdown">
              <a href="{{route('dashboard')}}" class="nav-link"><i class="fas fa-home"></i></i><span>Dashboard</span></a>
            </li>
            <li class="menu-header">Contents</li>
            <li class="dropdown
            @if (Request::segment(2) == 'posts')
            active
            @endif ">
              <a href="{{route('posts.index')}}" class="menu-toggle nav-link has-dropdown">
                <i class="fas fa-copy"></i>
                <span>Posts</span>
              </a>
              <ul class="dropdown-menu">
                <li>
                  <a href="{{route('posts.create')}}" class="nav-link"
                  @if (Request::segment(2) == 'posts' && Request::segment(3) == 'create')
                  style="color: #3e55ff"
                  @endif >New Post</a>
                </li>
                <li>
                  <a href="{{route('posts.index')}}" class="nav-link"
                  @if (Request::segment(2) == 'posts' && Request::segment(3) != 'create' && Request::segment(3) != 'trashed')
                  style="color: #3e55ff"
                  @endif >Posts</a>
                </li>
                <li>
                  <a href="{{route('trashedbox')}}" class="nav-link"
                  @if (Request::segment(2) == 'posts' && Request::segment(3) == 'trashed')
                  style="color: #3e55ff"
                  @endif >Trash</a>
                </li>
              </ul>
            </li>
            <li class="dropdown
            @if (Request::segment(2) == 'pages')
            active
            @endif ">
              <a href="{{route('pages.index')}}" class="menu-toggle nav-link has-dropdown">
                <i class="fas fa-file"></i>
                <span>Pages</span>
              </a>
              <ul class="dropdown-menu">
                <li>
                  <a href="{{route('pages.create')}}" class="nav-link"
                  @if (Request::segment(2) == 'pages' && Request::segment(3) == 'create')
                  style="color: #3e55ff"
                  @endif >New Page</a>
                </li>
                <li>
                  <a href="{{route('pages.index')}}" class="nav-link"
                  @if (Request::segment(2) == 'pages' && Request::segment(3) != 'create' && Request::segment(3) != 'trashed')
                  style="color: #3e55ff"
                  @endif >Pages</a>
                </li>
                <li>
                  <a href="{{route('pagetrashedbox')}}" class="nav-link"
                  @if (Request::segment(2) == 'pages' && Request::segment(3) == 'trashed')
                  style="color: #3e55ff"
                  @endif >Trash</a>
                </li>
              </ul>
            </li>
            <li class="dropdown
            @if (Request::segment(2) == 'categories')
            active
            @endif ">
              <a href="{{route('categories.index')}}" class="nav-link">
                <i class="fas fa-grip-vertical"></i>
                <span>Categories</span>
              </a>
            </li>
            <li class="dropdown
            @if (Request::segment(2) == 'gallery')
            active
            @endif ">
              <a href="{{route('gallery.index')}}" class="nav-link">
                <i class="fas fa-file-image"></i>
                <span>Gallery</span>
              </a>
            </li>
            <li class="dropdown
            @if (Request::segment(2) == 'comments')
            active
            @endif ">
              <a href="{{route('categories.index')}}" class="nav-link">
                <i class="fas fa-comments"></i>
                <span>Comments</span>
              </a>
            </li>
            <li class="dropdown
            @if (Request::segment(2) == 'settings')
            active
            @endif ">
              <a href="{{route('settings.index')}}" class="nav-link">
                <i class="fas fa-cog"></i>
                <span>Settings</span>
              </a>
            </li>
          </ul>
        </aside>
      </div>
      <!-- Main Content -->
      <div class="main-content">
        <section class="section">
          <div class="section-header">
            <h1>@yield('title')</h1>
          </div>

