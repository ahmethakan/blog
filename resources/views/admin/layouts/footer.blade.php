
    </div>
  </section>
</div>


</div>
<footer class="main-footer">
  <div class="footer-left">
    Copyrıght &copy; 2021 <div class="bullet"></div> Desıgn By <a href="@yield('site_address')"><b>Ahmet Hakan Çelik</b></a>

  </div>
  <div class="footer-right">
  </div>
</footer>
</div>
</div>
<!-- General JS Scripts -->
<script src="{{asset('front/')}}/js/app.min.js"></script>

<!-- JS Libraies -->
<script src="{{asset('front/')}}/bundles/prism/prism.js"></script>

<!-- Page Specific JS File -->
@yield('summernotejs')
<!-- Template JS File -->
<script src="{{asset('front/')}}/js/scripts.js"></script>
<!-- Custom JS File -->
<script src="{{asset('front/')}}/js/custom.js"></script>
</body>


</html>
