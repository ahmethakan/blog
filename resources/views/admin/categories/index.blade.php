@extends('admin.layouts.master')

@section('site_title', 'Blog')
@section('site_section', 'Categories')


@section('title', 'Categories')


@section('blog_name', 'AHC')
@section('site_address', 'http://127.0.0.1:8000')

@section('copyright', 'Copyright &copy; 2018 <div class="bullet"></div> All rights reserved.')

@section('dashboard')

<div class="section-body">
  <div class="row mt-4">
    <div class="col-12 col-md-12 col-lg-4">
      <div class="card">
        <div class="card-header">
          <h4>New Categories</h4>
        </div>
        <div class="card-body">
          <form method="post" action="{{route('categories.store')}}" enctype="multipart/form-data">
            @csrf
            <div class="form-group">
                <label>Name</label>
                <div class="">
                    <input type="text" class="form-control" name="categoryname" value="{{ old('categoryname') ?? '' }}" autocomplete="off">
                </div>
            </div>
            <div class="form-group">
                <label>Url</label>
                <div class="">
                    <input type="text" class="form-control" name="categoryslug" value="{{ old('categoryslug') ?? '' }}" autocomplete="off">
                </div>
            </div>
            <div class="form-group text-right">
                <button type="submit" class="btn btn-primary btn-lg btn-icon icon-right" name="submit">
                    Create
                </button>
            </div>
        </form>
        </div>
      </div>
    </div>
    <div class="col-12 col-md-12 col-lg-8">
      @if ($messages ?? '')
        <div class="alert alert-{{$messages['type'] ?? ''}} alert-dismissible show fade">
          <div class="alert-body">
            <button class="close" data-dismiss="alert">
              <span>×</span>
            </button>
            {{$messages['text'] ?? ''}}
          </div>
        </div>
      @endif
      <div class="card">
        <div class="card-header">
          <h4>All Categories</h4>
        </div>
        <div class="card-body">
          <div class="float-left">
            <select class="form-control selectric">
              <option>Action For Selected</option>
              <option>Move to Draft</option>
              <option>Move to Pending</option>
              <option>Delete Permanently</option>
            </select>
          </div>
          <div class="float-right">
            <form>
              <div class="input-group">
                <input type="text" class="form-control" placeholder="Search">
                <div class="input-group-append">
                  <button class="btn btn-primary"><i class="fas fa-search"></i></button>
                </div>
              </div>
            </form>
          </div>
          <div class="clearfix mb-3"></div>
          <div class="table-responsive">
            <table class="table table-striped">
              <tr>
                <th class="pt-2">
                  <div class="custom-checkbox custom-checkbox-table custom-control">
                    <input type="checkbox" data-checkboxes="mygroup" data-checkbox-role="dad"
                      class="custom-control-input" id="checkbox-all">
                    <label for="checkbox-all" class="custom-control-label">&nbsp;</label>
                  </div>
                </th>
                <th>Title</th>
                <th>Created At</th>
                <th>Posts</th>
                <th>Action</th>
              </tr>
              @foreach ($categories as $item)
              <tr>
                <td>
                  <div class="custom-checkbox custom-control">
                    <input type="checkbox" data-checkboxes="mygroup" class="custom-control-input"
                      id="{{$item->id}}">
                    <label for="{{$item->id}}" class="custom-control-label">&nbsp;</label>
                  </div>
                </td>
                <td>
                  <a href="{{route('categorypage',[$item->slug])}}">{{$item->name}}</a>
                </td>
                <td>
                  {{$item->created_at}}
                </td>
                <td>
                  {{$item->total}}
                </td>
                <td>
                    <a href="{{ route('categories.destroy', ['category'=> $item->id]) }}"
                      class="btn btn-icon btn-danger" onclick="event.preventDefault();
                    document.getElementById('delete-form-{{ $item->id }}').submit();">
                      <i class="fas fa-times"></i>
                    </a>
                    <form id="delete-form-{{ $item->id }}" action="{{ route('categories.destroy', ['category'=> $item->id]) }}"
                      method="POST" style="display: none;">
                      @method('DELETE');
                      @csrf
                    </form>
                </td>
              </tr>
              @endforeach
            </table>
          </div>
          <div class="float-right">
            <nav>
                {{$categories->links("pagination::bootstrap-4")}}
            </nav>
          </div>
        </div>

      </div>
    </div>
  </div>
</div>

@endsection
