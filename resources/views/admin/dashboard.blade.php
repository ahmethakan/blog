@extends('admin.layouts.master')

@section('site_title', 'Blog')
@section('site_section', 'Blog')


@section('title', 'Dashboard')


@section('blog_name', 'AHC')
@section('site_address', 'http://127.0.0.1:8000')

@section('copyright', 'Copyright &copy; 2018 <div class="bullet"></div> All rights reserved.')

@section('dashboard')
      <div class="section-body">
        <div class="row ">
          <div class="col-xl-3 col-lg-6">
            <div class="card l-bg-green">
              <div class="card-statistic-3">
                <div class="card-icon card-icon-large"><i class="fa fa-user"></i></div>
                <div class="card-content">
                  <h4 class="card-title">Total Users</h4>
                  <span>{{$user->getAllUsersCount()}}</span>
                  <div class="progress mt-1 mb-1" data-height="8">
                    <div class="progress-bar l-bg-purple" role="progressbar" data-width="{{$user->getAllUsersCount()}}%" aria-valuenow="{{$user->getAllUsersCount()}}"
                      aria-valuemin="0" aria-valuemax="100"></div>
                  </div>

                </div>
              </div>
            </div>
          </div>
          <div class="col-xl-3 col-lg-6">
            <div class="card l-bg-cyan">
              <div class="card-statistic-3">
                <div class="card-icon card-icon-large"><i class="fa fa-newspaper"></i></div>
                <div class="card-content">
                  <h4 class="card-title">Total Posts</h4>
                  <span>{{$user->getAllPostsCount()}}</span>
                  <div class="progress mt-1 mb-1" data-height="8">
                    <div class="progress-bar l-bg-orange" role="progressbar" data-width="{{$user->getAllPostsCount()}}%" aria-valuenow="{{$user->getAllPostsCount()}}"
                      aria-valuemin="0" aria-valuemax="100"></div>
                  </div>
                </div>
              </div>
            </div>
          </div>
          <div class="col-xl-3 col-lg-6">
            <div class="card l-bg-purple">
              <div class="card-statistic-3">
                <div class="card-icon card-icon-large"><i class="fa fa-comment"></i></div>
                <div class="card-content">
                  <h4 class="card-title">Total Comments</h4>
                  <span>10,225</span>
                  <div class="progress mt-1 mb-1" data-height="8">
                    <div class="progress-bar l-bg-cyan" role="progressbar" data-width="25%" aria-valuenow="25"
                      aria-valuemin="0" aria-valuemax="100"></div>
                  </div>
                </div>
              </div>
            </div>
          </div>
          <div class="col-xl-3 col-lg-6">
            <div class="card l-bg-orange">
              <div class="card-statistic-3">
                <div class="card-icon card-icon-large"><i class="fa fa-heart"></i></div>
                <div class="card-content">
                  <h4 class="card-title">Total Likes</h4>
                  <span>2,658</span>
                  <div class="progress mt-1 mb-1" data-height="8">
                    <div class="progress-bar l-bg-green" role="progressbar" data-width="25%" aria-valuenow="25"
                      aria-valuemin="0" aria-valuemax="100"></div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
@endsection
