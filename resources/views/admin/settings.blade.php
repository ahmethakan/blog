@extends('admin.layouts.master')

@section('site_title', 'Blog')
@section('site_section', 'Settings')

@section('title', 'Settings')

@section('blog_name', 'AHC')
@section('site_address', 'http://127.0.0.1:8000')

@section('copyright', 'Copyright &copy; 2018 <div class="bullet"></div> All rights reserved.')

@section('dashboard')

<div class="section-body">
    <div class="row mt-sm-4">
        <div class="col-12 col-md-12 col-lg-4">
          <div class="card author-box">
            <div class="card-body">
              <div class="author-box-center">
                <img alt="{{$user->name}}" src="{{$user->picture}}" class="rounded-circle author-box-picture">
                <div class="clearfix"></div>
                <div class="author-box-name">
                  <a href="#">{{$user->name}}</a>
                </div>
                <div class="author-box-job">{{$user->job}}</div>
              </div>
              <div class="text-center">
                <div class="author-box-description">
                  <p>
                    {{$user->about}}
                  </p>
                </div>
                <div class="w-100 d-sm-none"></div>
              </div>
            </div>
          </div>
          <div class="card">
            <div class="card-header">
              <h4>Personal Details</h4>
            </div>
            <div class="card-body">
              <div class="py-4">
                <p class="clearfix">
                  <span class="float-left">
                    Birthday
                  </span>
                  <span class="float-right text-muted">
                    30-05-1998
                  </span>
                </p>
                <p class="clearfix">
                  <span class="float-left">
                    Phone
                  </span>
                  <span class="float-right text-muted">
                    {{$user->phone}}
                  </span>
                </p>
                <p class="clearfix">
                  <span class="float-left">
                    Mail
                  </span>
                  <span class="float-right text-muted">
                    {{$user->email}}
                  </span>
                </p>
                <p class="clearfix">
                  <span class="float-left">
                    Facebook
                  </span>
                  <span class="float-right text-muted">
                    <a href="#">John Deo</a>
                  </span>
                </p>
                <p class="clearfix">
                  <span class="float-left">
                    Twitter
                  </span>
                  <span class="float-right text-muted">
                    <a href="#">@johndeo</a>
                  </span>
                </p>
              </div>
            </div>
          </div>
          <div class="card">
            <div class="card-header">
              <h4>Skills</h4>
            </div>
            <div class="card-body">
              <ul class="list-unstyled user-progress list-unstyled-border list-unstyled-noborder">
                <li class="media">
                  <div class="media-body">
                    <div class="media-title">Java</div>
                  </div>
                  <div class="media-progressbar p-t-10">
                    <div class="progress" data-height="6">
                      <div class="progress-bar bg-primary" data-width="70%"></div>
                    </div>
                  </div>
                </li>
                <li class="media">
                  <div class="media-body">
                    <div class="media-title">Web Design</div>
                  </div>
                  <div class="media-progressbar p-t-10">
                    <div class="progress" data-height="6">
                      <div class="progress-bar bg-warning" data-width="80%"></div>
                    </div>
                  </div>
                </li>
                <li class="media">
                  <div class="media-body">
                    <div class="media-title">Photoshop</div>
                  </div>
                  <div class="media-progressbar p-t-10">
                    <div class="progress" data-height="6">
                      <div class="progress-bar bg-green" data-width="48%"></div>
                    </div>
                  </div>
                </li>
              </ul>
            </div>
          </div>
        </div>
        <div class="col-12 col-md-12 col-lg-8">
          <div class="card">
            <div class="padding-20">
              <ul class="nav nav-tabs" id="myTab2" role="tablist">
                <li class="nav-item">
                  <a class="nav-link active" id="home-tab2" data-toggle="tab" href="#profile" role="tab"
                    aria-selected="true">Profile</a>
                </li>
                <li class="nav-item">
                  <a class="nav-link" id="profile-tab2" data-toggle="tab" href="#settings" role="tab"
                    aria-selected="false">Settings</a>
                </li>
              </ul>
              <div class="tab-content tab-bordered" id="myTab3Content">
                <div class="tab-pane fade show active" id="profile" role="tabpanel" aria-labelledby="home-tab2">
                  <form method="POST" action="{{route('user.update', ['id'=> $user->id])}}" enctype="multipart/form-data" class="needs-validation">
                    @csrf
                    <div class="card-header">
                      <h4>Profile</h4>
                    </div>
                    <div class="card-body">
                      <div class="row">
                        <div class="form-group col-md-6 col-12">
                          <label>IP Address</label>
                          <input type="text" class="form-control" value="{{$user->ip_address}}" name="user_ip_address">
                        </div>
                        <div class="form-group col-md-6 col-12">
                          <label>Name</label>
                          <input type="text" class="form-control" value="{{$user->name}}" name="user_name">
                        </div>
                      </div>
                      <div class="row">
                        <div class="form-group col-md-6 col-12">
                          <label>Author Name</label>
                          <input type="text" class="form-control" value="{{$user->authorname}}" name="user_authorname">
                        </div>
                        <div class="form-group col-md-6 col-12">
                          <label>User Name</label>
                          <input type="text" class="form-control" value="{{$user->username}}" name="user_username">
                        </div>
                      </div>
                      <div class="row">
                        <div class="form-group col-md-6 col-12">
                          <label>Email</label>
                          <input type="email" class="form-control" value="{{$user->email}}" name="user_email">
                        </div>
                        <div class="form-group col-md-6 col-12">
                          <label>Password</label>
                          <input type="password" class="form-control" value="{{$user->password}}" name="user_password">
                        </div>
                      </div>
                      <div class="row">
                        <div class="form-group col-12">
                          <label>Picture</label>
                          <input type="text" class="form-control" value="{{$user->picture}}" name="user_picture">
                        </div>
                      </div>
                      <div class="row">
                        <div class="form-group col-12">
                          <label>About</label>
                          <input type="text" class="form-control" value="{{$user->about}}" name="user_about">
                        </div>
                      </div>
                      <div class="row">
                        <div class="form-group col-md-6 col-12">
                          <label>Phone</label>
                          <input type="text" class="form-control" value="{{$user->phone}}" name="user_phone">
                        </div>
                        <div class="form-group col-md-6 col-12">
                          <label>Job</label>
                          <input type="text" class="form-control" value="{{$user->job}}" name="user_job">
                        </div>
                      </div>
                      <div class="row">
                        <div class="form-group col-12">
                          <label>Location</label>
                          <input type="text" class="form-control" value="{{$user->location}}" name="user_location">
                        </div>
                      </div>
                    </div>
                    <div class="card-footer text-right">
                      <button type="submit" class="btn btn-primary">Save Changes</button>
                    </div>
                  </form>
                </div>
                <div class="tab-pane fade" id="settings" role="tabpanel" aria-labelledby="profile-tab2">
                  <form method="POST" action="{{route('settings.store')}}" enctype="multipart/form-data" class="needs-validation">
                    @csrf
                    <div class="card-header">
                      <h4>Settings</h4>
                    </div>
                    <div class="card-body">
                      <div class="row">
                        <div class="form-group col-md-6 col-12">
                          <label>Title</label>
                          <input type="text" class="form-control" value="{{$settings->site_title}}" name="site_title">
                        </div>
                        <div class="form-group col-md-6 col-12">
                          <label>Address</label>
                          <input type="text" class="form-control" value="{{$settings->site_address}}" name="site_address">
                        </div>
                      </div>
                      <div class="row">
                        <div class="form-group col-12">
                          <label>Description</label>
                          <textarea
                            class="form-control summernote-simple" name="site_description">{{$settings->site_description}}</textarea>
                        </div>
                      </div>
                      <div class="row">
                        <div class="form-group col-12">
                          <label>Tags</label>
                          <input type="text" class="form-control" value="{{$settings->site_tags}}" name="site_tags">
                        </div>
                      </div>
                      <div class="row">
                        <div class="form-group col-12">
                          <label>Copyright</label>
                          <input type="text" class="form-control" value="{{$settings->site_copyright}}" name="site_copyright">
                        </div>
                      </div>
                      <div class="row">
                        <div class="form-group col-md-6 col-12">
                          <label>Favicon</label>
                          <input type="text" class="form-control" value="{{$settings->site_favicon}}" name="site_favicon">
                        </div>
                        <div class="form-group col-md-6 col-12">
                          <label>Blog name</label>
                          <input type="text" class="form-control" value="{{$settings->blog_name}}" name="blog_name">
                        </div>
                      </div>
                    </div>
                    <div class="card-footer text-right">
                      <button type="submit" class="btn btn-primary">Save Changes</button>
                    </div>
                  </form>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
</div>

@endsection
