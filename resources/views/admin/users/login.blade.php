@section('site_title', 'Blog')
@section('site_section', 'Blog')
@section('title', 'Login')
@section('blog_name', 'AHC')
@section('site_address', 'http://127.0.0.1:8000')

@section('copyright', 'Copyright &copy; 2018 <div class="bullet"></div> All rights reserved.')

<!DOCTYPE html>
<html lang="tr">
<head>
  <meta charset="UTF-8">
  <meta content="width=device-width, initial-scale=1, maximum-scale=1, shrink-to-fit=no" name="viewport">
  <title>@yield('title') -  @yield('site_title')</title>
  <!-- General CSS Files -->
  <link rel="stylesheet" href="{{asset('front/')}}/css/app.min.css">
  <link rel="stylesheet" href="{{asset('front/')}}/bundles/bootstrap-social/bootstrap-social.css">
  <!-- Template CSS -->
  <link rel="stylesheet" href="{{asset('front/')}}/css/style.css">
  <link rel="stylesheet" href="{{asset('front/')}}/css/components.css">
  <!-- Custom style CSS -->
  <link rel="stylesheet" href="{{asset('front/')}}/css/custom.css">
  <link rel='shortcut icon' type='image/x-icon' href='{{asset('front/')}}/img/favicon.ico' />
</head>

<body>
  <div class="loader"></div>
  <div id="app">
    <section class="section">
      <div class="container mt-5">
        <div class="row">
          <div class="col-12 col-sm-8 offset-sm-2 col-md-6 offset-md-3 col-lg-6 offset-lg-3 col-xl-4 offset-xl-4">
            @foreach ($errors->all() as $error)
              <div class="alert alert-danger alert-dismissible show fade">
                <div class="alert-body">
                  <button class="close" data-dismiss="alert">
                    <span>×</span>
                  </button>
                  {{$error}}
                </div>
              </div>
            @endforeach
            <div class="card card-primary">
              <div class="card-header">
                <h4>
                  @yield('title')
                </h4>
                <div class="card-header-action">
                  <a href="{{route('create.user')}}" class="btn btn-primary">
                    Create
                  </a>
                  <a class="btn btn-icon btn-danger" href="{{route('homepage')}}"><i class="fas fa-times"></i></a>
                </div>
              </div>
              <div class="card-body">

                <form method="POST" action="{{route('login.post')}}" class="needs-validation" novalidate="">
                  @csrf
                  <div class="form-group">
                    <label for="blg_email">Email</label>
                    <input id="blg_email" type="email" class="form-control" name="blg_email" tabindex="1" required autofocus>
                    <div class="invalid-feedback">
                      Please fill in your email
                    </div>
                  </div>
                  <div class="form-group">
                    <div class="d-block">
                      <label for="blg_password" class="control-label">Password</label>
                      <div class="float-right">
                        <a href="auth-forgot-password.html" class="text-small">
                          Forgot Password?
                        </a>
                      </div>
                    </div>
                    <input id="blg_password" type="password" class="form-control" name="blg_password" tabindex="2" required>
                    <div class="invalid-feedback">
                      please fill in your password
                    </div>
                  </div>
                  <div class="form-group">
                    <div class="custom-control custom-checkbox">
                      <input type="checkbox" name="remember" class="custom-control-input" tabindex="3" id="remember-me">
                      <label class="custom-control-label" for="remember-me">Remember Me</label>
                    </div>
                  </div>
                  <div class="form-group">
                    <button type="submit" class="btn btn-primary btn-lg btn-block" tabindex="4">
                      Login
                    </button>
                  </div>
                </form>
              </div>
            </div>
          </div>
        </div>
      </div>
    </section>
  </div>
  <!-- General JS Scripts -->
  <script src="{{asset('front/')}}/js/app.min.js"></script>
  <!-- JS Libraies -->
  <!-- Page Specific JS File -->
  <!-- Template JS File -->
  <script src="{{asset('front/')}}/js/scripts.js"></script>
  <!-- Custom JS File -->
  <script src="{{asset('front/')}}/js/custom.js"></script>
</body>
</html>
{{--
            @foreach ($errors->all() as $error)
            <div class="alert alert-danger alert-dismissible show fade">
              <div class="alert-body">
                <button class="close" data-dismiss="alert">
                  <span>×</span>
                </button>
                {{$error}}
              </div>
            </div>
            @endforeach
            <form method="POST" action="{{route('login.post')}}" class="needs-validation" novalidate="">
              @csrf
              <div class="form-group">
                <label for="email">Email</label>
                <input id="email" type="email" class="form-control" name="blg_email" tabindex="1" required autofocus>
                <div class="invalid-feedback">
                  Please fill in your email
                </div>
              </div>

              <div class="form-group">
                <div class="d-block">
                  <label for="password" class="control-label">Password</label>
                </div>
                <input id="password" type="password" class="form-control" name="blg_password" tabindex="2" required>
                <div class="invalid-feedback">
                  please fill in your password
                </div>
              </div>

              <div class="form-group">
                <div class="custom-control custom-checkbox">
                  <input type="checkbox" name="remember" class="custom-control-input" tabindex="3" id="remember-me">
                  <label class="custom-control-label" for="remember-me">Remember Me</label>
                </div>
              </div>

              <div class="form-group text-right">
                <button type="submit" class="btn btn-primary btn-lg btn-icon icon-right" tabindex="4">
                  Login
                </button>
              </div>
            </form>
          </div>
        </div>

      </div>
    </section>
  </div> --}}
