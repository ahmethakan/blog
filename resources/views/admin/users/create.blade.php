@section('site_title', 'Blog')
@section('site_section', 'Blog')
@section('title', 'Create')
@section('blog_name', 'AHC')
@section('site_address', 'http://127.0.0.1:8000')

@section('copyright', 'Copyright &copy; 2018 <div class="bullet"></div> All rights reserved.')

<!DOCTYPE html>
<html lang="tr">
<head>
  <meta charset="UTF-8">
  <meta content="width=device-width, initial-scale=1, maximum-scale=1, shrink-to-fit=no" name="viewport">
  <title>@yield('title') -  @yield('site_title')</title>
  <!-- General CSS Files -->
  <link rel="stylesheet" href="{{asset('front/')}}/css/app.min.css">
  <link rel="stylesheet" href="{{asset('front/')}}/bundles/bootstrap-social/bootstrap-social.css">
  <!-- Template CSS -->
  <link rel="stylesheet" href="{{asset('front/')}}/css/style.css">
  <link rel="stylesheet" href="{{asset('front/')}}/css/components.css">
  <!-- Custom style CSS -->
  <link rel="stylesheet" href="{{asset('front/')}}/css/custom.css">
  <link rel='shortcut icon' type='image/x-icon' href='{{asset('front/')}}/img/favicon.ico' />

  <script src="{{asset('front/')}}/js/jquery-3.6.0.min.js"></script>
  <meta name="csrf-token" content="{{ csrf_token() }}" />
</head>

<body>
  <div class="loader"></div>
  <div id="app">
    <section class="section">
      <div class="container mt-5">
        <div class="row">
          <div class="col-12 col-sm-8 offset-sm-2 col-md-6 offset-md-3 col-lg-6 offset-lg-3 col-xl-4 offset-xl-4">
            <div class="alert alert-dismissible fade" id="buttonAlert">
              <div class="alert-body">
              </div>
            </div>
            <div class="card card-primary">
              <div class="card-header">
                <h4>
                  @yield('title')
                </h4>
                <div class="card-header-action">
                  <a href="{{route('login')}}" class="btn btn-primary">
                    Login
                  </a>
                  <a class="btn btn-icon btn-danger" href="{{route('homepage')}}"><i class="fas fa-times"></i></a>
                </div>
              </div>
              <div class="card-body">
                <form id="ajaxform" class="needs-validation" novalidate="">
                  <div class="form-group">
                    <label for="blg_email">Name</label>
                    <input id="blg_name" type="text" class="form-control" name="blg_name" tabindex="1" required>
                  </div>
                  <div class="form-group">
                    <label for="blg_email">Email</label>
                    <input id="blg_email" type="email" class="form-control" name="blg_email" tabindex="2" required>
                  </div>
                  <div class="form-group">
                    <label for="blg_phone">Phone</label>
                    <input id="blg_phone" type="text" class="form-control" name="blg_phone" tabindex="3" required>
                  </div>
                  <div class="form-group">
                    <label for="blg_phone">Password</label>
                    <input id="blg_password" type="password" class="form-control" name="blg_password" tabindex="4" required>
                  </div>
                  <div class="form-group">
                    <button type="submit" class="btn btn-primary btn-lg btn-block save-data" tabindex="5">
                      Create
                    </button>
                  </div>
                </form>
              </div>
            </div>
          </div>
        </div>
      </div>
    </section>
  </div>

  <script>

    $(".save-data").click(function(event)
    {
        event.preventDefault();

        let name = $("input[name=blg_name]").val();
        let email = $("input[name=blg_email]").val();
        let mobile_number = $("input[name=blg_phone]").val();
        let password = $("input[name=blg_password]").val();
        let _token = $('meta[name="csrf-token"]').attr('content');

        let i = 0;

        if (name.length < 3) {
          $("#blg_name").addClass('is-invalid');
          i = i + 1;
        }
        else
        {
          var element = document.getElementById("blg_name");
          element.classList.remove("is-invalid");
        }

        if (email.length < 7) {
          $("#blg_email").addClass('is-invalid');
          i = i + 1;
        }
        else
        {
          var element = document.getElementById("blg_email");
          element.classList.remove("is-invalid");
        }

        if (mobile_number.length < 11) {
          $("#blg_phone").addClass('is-invalid');
          i = i + 1;
        }
        else
        {
          var element = document.getElementById("blg_phone");
          element.classList.remove("is-invalid");
        }

        if (password.length < 8) {
          $("#blg_password").addClass('is-invalid');
          i = i + 1;
        }
        else
        {
          var element = document.getElementById("blg_password");
          element.classList.remove("is-invalid");
        }

        if( i == 0 )
        {
          $.ajax({
            url: "/user/create",
            type:"POST",
            data:{
              name:name,
              email:email,
              mobile_number:mobile_number,
              password:password,
              _token: _token
            },
            success:function(response)
            {
              console.log(response);
              if( response.success == 'true' )
              {
                $('.alert-body').text(response.message);
                var element = document.getElementById("buttonAlert");
                element.classList.remove("alert-danger");
                $("#buttonAlert").addClass('alert-success show');
                $("#ajaxform")[0].reset();
              }
              else
              {
                var element = document.getElementById("buttonAlert");
                element.classList.remove("alert-success");
                $('.alert-body').text(response.message);
                $("#buttonAlert").addClass('alert-danger show');
              }
            }
          });
        }

    });
  </script>

  <!-- General JS Scripts -->
  <script src="{{asset('front/')}}/js/app.min.js"></script>
  <!-- JS Libraies -->
  <!-- Page Specific JS File -->
  <!-- Template JS File -->
  <script src="{{asset('front/')}}/js/scripts.js"></script>
  <!-- Custom JS File -->
  <script src="{{asset('front/')}}/js/custom.js"></script>
</body>
</html>