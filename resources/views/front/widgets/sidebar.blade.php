<div class="col-12 col-md-3 col-lg-2">
  <div class="sticky">
  <div class="col-12 col-md-12 col-lg-12">
    <div class="author-box-name">
      <a href="{{route('homepage')}}" style="text-decoration: none;">
        <h5>Ahmet H. Çelik</h5>
      </a>
    </div>
    <div class="author-box-job">C/C++ Developer</div>
    <br>
    <a href="https://github.com/AHakan" class="btn btn-social-icon btn-sm mr-1 btn-github">
      <i class="fab fa-github"></i>
    </a>
    <a href="https://www.linkedin.com/in/ahc0/" class="btn btn-social-icon btn-sm mr-1 btn-linkedin">
      <i class="fab fa-linkedin"></i>
    </a>
    @if ( Auth::user() )
    <a href="{{route('dashboard')}}" class="btn btn-social-icon btn-sm mr-1">
      @if ( Auth::user()->picture == NULL )
      <img alt="image" src="{{asset('front/')}}/img/favicon.ico">
      @else
      <img alt="image" src="{{ Auth::user()->picture }}">
      @endif
    </a>
    @else
    <a href="{{route('login')}}" class="btn btn-social-icon btn-sm mr-1 btn-openid">
      <i class="fas fa-user"></i>
    </a>
    @endif
    <br>
    <br>

    @if ( isset($_COOKIE['mode']) && $_COOKIE['mode'] == 1 )
    <form class="needs-validation" novalidate="">
      <div class="form-group">
        <button type="submit" class="btn btn-icon icon-left btn-light btn-sm dark-mode" name="mode" value="1">
          <i class="fas fa-sun"></i> Light Mode
        </button>
      </div>
    </form>
    @else
    <form class="needs-validation" novalidate="">
      <div class="form-group">
        <button type="submit" class="btn btn-icon icon-left btn-dark btn-sm dark-mode" name="mode" value="1">
          <i class="fas fa-moon"></i> Dark Mode
        </button>
      </div>
    </form>
    @endif

    </div>
  </div>
  <br>

  <div class="nonsticky">
    <div class="col-12 col-md-12 col-lg-12">
      <div class="author-box-name">
        <a href="{{route('homepage')}}" style="text-decoration: none;">
          <h4>Ahmet H. Çelik</h4>
        </a>
      </div>
      <div class="author-box-job">C/C++ Developer</div>
      <br>
      <a href="https://github.com/AHakan" class="btn btn-social-icon btn-sm mr-1 btn-github">
        <i class="fab fa-github"></i>
      </a>
      <a href="https://www.linkedin.com/in/ahc0/" class="btn btn-social-icon btn-sm mr-1 btn-linkedin">
        <i class="fab fa-linkedin"></i>
      </a>
      @if ( Auth::user() )
      <a href="{{route('dashboard')}}" class="btn btn-social-icon btn-sm mr-1">
        @if ( Auth::user()->picture == NULL )
        <img alt="image" src="{{asset('front/')}}/img/favicon.ico">
        @else
        <img alt="image" src="{{ Auth::user()->picture }}">
        @endif
      </a>
      @else
      <a href="{{route('login')}}" class="btn btn-social-icon btn-sm mr-1 btn-openid">
        <i class="fas fa-user"></i>
      </a>
      @endif
      <br>
      <br>

      @if ( isset($_COOKIE['mode']) && $_COOKIE['mode'] == 1 )
      <form class="needs-validation" novalidate="">
        <div class="form-group">
          <button type="submit" class="btn btn-icon icon-left btn-light btn-sm dark-mode" name="mode" value="1">
            <i class="fas fa-sun"></i> Light Mode
          </button>
        </div>
      </form>
      @else
      <form class="needs-validation" novalidate="">
        <div class="form-group">
          <button type="submit" class="btn btn-icon icon-left btn-dark btn-sm dark-mode" name="mode" value="1">
            <i class="fas fa-moon"></i> Dark Mode
          </button>
        </div>
      </form>
      @endif
      </div>
    </div>
    <br>
</div>

<script>

  $(".dark-mode").click(function(event)
  {
      event.preventDefault();

      // console.log("Merhaba");

      let _token = $('meta[name="csrf-token"]').attr('content');

      $.ajax({
        url: "/settings/mode",
        type:"POST",
        data:{
          mode:1,
          _token: _token
        },
        success:function(response)
        {
          console.log(response);
          location.reload();
        }
      });

  });

  </script>