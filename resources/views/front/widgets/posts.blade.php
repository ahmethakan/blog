@if (count($posts) >0)
  <div class="col-0 col-md-1 col-lg-1">
  </div>
  @include('front.widgets.sidebar')

  <div class="col-12 col-md-8 col-lg-6">
  @foreach ($posts as $post)
    <div class="col-12 col-md-12 col-lg-12">
      <small>{{$post->created_at->diffForHumans()}}</small>
        <a href="{{route('singlepost',[ $post->getCategory->slug, $post->slug])}}" style="text-decoration: none;">
          <h5>{{$post->title}}</h5>
        </a>
    </div>
  @endforeach
  @if ($posts->hasPages())
  <br>
    <div class="float-right">
      <nav>
          {{-- {{$posts->links("pagination::bootstrap-4")}} --}}
          @if( $posts->onFirstPage() == 1 )

          @endif
          <a href="{{$posts->previousPageUrl()}} " style="text-decoration: none;">
            <i class="fas fa-angle-left"></i>
            <b>Previous</b>
          </a>
          <div class="bullet"></div>

          <a href="{{$posts->nextPageUrl()}} " style="text-decoration: none;">
            <b>Next</b>
            <i class="fas fa-angle-right"></i>
          </a>

      </nav>
    </div>
  @endif
  </div>

  <div class="col-0 col-md-0 col-lg-1">
  </div>

@endif