@extends('front.layouts.master')

@section('site_title', 'Blog')
@section('site_section', 'Blog')

@if ($posts->currentPage() == 1)
  @section('title', 'Homepage')
@else
  @section('title', 'Page '.$posts->currentPage())
@endif

@section('blog_name', 'AHC')
@section('site_address', 'http://127.0.0.1:8000')

@section('copyright', 'Copyright &copy; 2018 <div class="bullet"></div> All rights reserved.')

@section('content')
  <!-- Main Content -->
  <div class="main-content">
    <section class="section">
      <div class="section-body">
        <div class="row">
          @include('front.widgets.posts')
        </div>
      </div>
    </section>
  </div>
@endsection
