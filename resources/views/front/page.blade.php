@extends('front.layouts.master')

@section('site_title', 'Blog')
@section('site_section', 'Page')
@section('title', Str::ucfirst($content->title))
@section('blog_name', 'AHC')
@section('site_address', 'http://127.0.0.1:8000')

@section('postmetatags')
<meta name="description" content="{{$content->description}}">
<meta name="keywords" content="{{$content->tags}}">
@endsection

@section('content')
<!-- Main Content -->
<div class="main-content">
  <section class="section">
    <div class="section-body">
      <div class="row">
        <div class="col-0 col-md-0 col-lg-1"> </div>
        @include('front.widgets.sidebar')
        <div class="col-12 col-md-8 col-lg-6">
          <div class="col-12 col-md-12 col-lg-12">
            <small>
              <a href="{{route('homepage')}}" style="text-decoration: none;">
                <i class="fas fa-angle-left"> </i>
                <b>Go back</b>
              </a>
            </small>
            <div class="bullet"></div>
            <i class="far fa-hourglass"> </i>
            <small>{{$content->created_at->diffForHumans()}}</small>
            <h5>
              <b>{{Str::ucfirst($content->title)}}</b>
            </h5>
            @if ($content->tumbnail)
              <img alt="{{$content->tumbnail}}" src="{{$content->tumbnail}}" class="img-fluid center" style="padding: 0px 0px 10px 0px">
            @endif
            <p class="mb-2">
              {!!$content->content!!}
            </p>
          </div>
        </div>
      </div>
    </div>
  </section>
</div>
@endsection
