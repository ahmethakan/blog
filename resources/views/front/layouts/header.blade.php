<!DOCTYPE html>
<html lang="tr">
<head>
  <meta charset="UTF-8">
  <meta content="width=device-width, initial-scale=1, maximum-scale=1, shrink-to-fit=no" name="viewport">
  <title>@yield('title') -  @yield('site_title')</title>
  @yield('postmetatags')
  <!-- General CSS Files -->
  <link rel="stylesheet" href="{{asset('front/')}}/css/app.min.css">
  <link rel="stylesheet" href="{{asset('front/')}}/bundles/bootstrap-social/bootstrap-social.css">
  <!-- Template CSS -->
  <link rel="stylesheet" href="{{asset('front/')}}/css/style1.css">
  <!-- Custom style CSS -->
  <link rel="stylesheet" href="{{asset('front/')}}/css/custom.css">
  <link rel='shortcut icon' type='image/x-icon' href='{{asset('front/')}}/img/favicon.ico' />

  <script src="{{asset('front/')}}/js/jquery-3.6.0.min.js"></script>
  <meta name="csrf-token" content="{{ csrf_token() }}" />

  @if( isset($_COOKIE['mode']) && $_COOKIE['mode'] == 1 )
  <style>body{background-color: #111;color: #f9f9f9;}p{color: #f9f9f9;}</style>
  @else
  <style>body{background-color: #f9f9f9;}p{color: #333;}</style>
  @endif

</head>

<body>
  <div class="loader"></div>