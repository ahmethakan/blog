@extends('front.layouts.master')

@section('site_title', 'Blog')
@section('site_section', 'Post')
@section('title', $post->title)
@section('blog_name', 'AHC')
@section('site_address', 'http://127.0.0.1:8000')

@section('postmetatags')
<meta name="description" content="{{$post->description}}">
<meta name="keywords" content="{{$post->tags}}">
<meta name="author" content="{{$post->getAuthor->authorname}}">
@endsection

@section('content')
<!-- Main Content -->
<div class="main-content">
  <section class="section">
    <div class="section-body">

      <div class="row">
        <div class="col-0 col-md-0 col-lg-1"> </div>
        @include('front.widgets.sidebar')
        <div class="col-12 col-md-8 col-lg-6">
          <div class="col-12 col-md-12 col-lg-12">
            @if ( $post->status == 0 )
            <div class="alert alert-warning alert-has-icon">
              <div class="alert-icon"><i class="far fa-lightbulb"></i></div>
              <div class="alert-body">
                <div class="alert-title">Warning</div>
                This post has not been published.
              </div>
            </div>
            @endif
            <small><a href="{{route('homepage')}}" style="text-decoration: none;">
              <i class="fas fa-angle-left"> </i>
              Go back
            </a>
            </small>
            <div class="bullet"></div>
            <i class="far fa-list-alt"></i>
            <small>{{$post->getCategory->name}}</small>
            <div class="bullet"></div>
            <i class="far fa-hourglass"> </i>
            <small>{{$post->created_at->diffForHumans()}}</small>
            <h4>
              {{$post->title}}
            </h4>
            @if ($post->tumbnail)
              <img alt="{{$post->title}}" src="{{$post->tumbnail}}" class="img-fluid center" style="padding: 0px 0px 10px 0px">
            @endif
            <p class="mb-2">
              {!!$post->content!!}
            </p>
          </div>
        </div>
      </div>
    </div>
  </section>
</div>
@endsection
