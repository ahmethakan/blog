<?php

use Illuminate\Support\Facades\Route;

use App\Http\Controllers\Front\Homepage;
use App\Http\Controllers\Admin\Dashboard;
use App\Http\Controllers\Admin\Posts;
use App\Http\Controllers\Admin\Categories;
use App\Http\Controllers\Admin\Pages;
use App\Http\Controllers\Admin\Gallery;
use App\Http\Controllers\Admin\Settings;



/*
|--------------------------------------------------------------------------
| Backend Routes
|--------------------------------------------------------------------------
*/

Route::prefix('user')->middleware('check.login')->group(function ()
{
    Route::get(
        '/login',
        [Dashboard::class, 'login']
    )->name('login');

    Route::post(
        '/login',
        [Dashboard::class, 'loginPost']
    )->name('login.post');

    Route::get(
        '/create',
        [Dashboard::class, 'create']
    )->name('create.user');

    Route::post(
        '/create',
        [Dashboard::class, 'createUserPost']
    )->name('create.user.post');
});

Route::prefix('user')->middleware('check.auth')->group(function ()
{
    Route::get(
        '/dashboard',
        [Dashboard::class, 'index']
    )->name('dashboard');

    Route::get(
        '/logout',
        [Dashboard::class, 'logout']
    )->name('logout');

    Route::post(
        '/update/user/{id}',
        [Dashboard::class, 'update']
    )->name('user.update');


    // Post Routes
    // ------------------- //
    Route::delete(
        '/posts/{id}/delete',
        [Posts::class, 'delete']
    )->name('postdelete');

    Route::get(
        '/posts/trashed',
        [Posts::class, 'trashed']
    )->name('trashedbox');

    Route::resource(
        '/posts',
        Posts::class
    );
    // ------------------- //


    // Category Routes
    // ------------------- //

    Route::resource(
        '/categories',
        Categories::class
    );
    // ------------------- //


    // Page Routes
    // ------------------- //
    Route::delete(
        '/pages/{id}/delete',
        [Pages::class, 'delete']
    )->name('pagedelete');

    Route::get(
        '/pages/trashed',
        [Pages::class, 'trashed']
    )->name('pagetrashedbox');

    Route::resource(
        '/pages',
        Pages::class
    );
    // ------------------- //

    // Gallery Routes
    // ------------------- //

    Route::resource(
        '/gallery',
        Gallery::class
    );

    // ------------------- //

    // Setting Routes
    // ------------------- //

    Route::resource(
        '/settings',
        Settings::class
    );

    // ------------------- //
});

/*
|--------------------------------------------------------------------------
| Front Routes
|--------------------------------------------------------------------------
*/

Route::get(
    '/',
    [Homepage::class, 'index']
)->name('homepage');

Route::post(
    '/settings/mode',
    [Homepage::class, 'changeMode']
)->name('changeMode');

Route::get(
    '/{ctg}',
    [Homepage::class, 'categorypage']
)->name('categorypage');

Route::get(
    '/page/{slug}',
    [Homepage::class, 'pagepage']
)->name('pagepage');

Route::get(
    '/{ctg}/{slug}',
    [Homepage::class, 'singlepage']
)->name('singlepost');

